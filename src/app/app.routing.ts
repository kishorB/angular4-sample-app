import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AppComponent } from './app.component';

const routes: Routes = [
  { path: 'sms', loadChildren: './lazy-components/sms/sms.module#SmsModule'},
];

export const routing: ModuleWithProviders = RouterModule.forRoot(routes);
