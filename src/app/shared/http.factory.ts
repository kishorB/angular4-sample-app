import { XHRBackend, Http, RequestOptions } from '@angular/http';
import { InterceptedHttp } from './http.interceptor';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

export function httpFactory(xhrBackend: XHRBackend, requestOptions: RequestOptions ,
   toastrService: ToastrService , router: Router): Http {
  return new InterceptedHttp(xhrBackend, requestOptions , toastrService , router);
}
