import { Injectable , Inject , forwardRef} from '@angular/core';
import { ConnectionBackend, RequestOptions, Request, RequestOptionsArgs, Response, Http, Headers } from '@angular/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { ToastrService } from 'ngx-toastr';

@Injectable()
export class InterceptedHttp extends Http {
   constructor(backend: ConnectionBackend, defaultOptions: RequestOptions , private toastrService: ToastrService , private router: Router) {
     super(backend, defaultOptions);
  }

  public request(url: string | Request, options?: RequestOptionsArgs): Observable<Response> {
    return super.request(url, options).catch(this.catchErrors());
  }

  public get(url: string, options?: RequestOptionsArgs): Observable<Response> {
    url = this.updateUrl(url);
    return super.get(url, this.getRequestOptionArgs(options)).catch(this.catchErrors());
  }

  public post(url: string, body: string, options?: RequestOptionsArgs): Observable<Response> {
    url = this.updateUrl(url);
    return super.post(url, body, this.getRequestOptionArgs(options)).catch(this.catchErrors());
  }

  public put(url: string, body: string, options?: RequestOptionsArgs): Observable<Response> {
    url = this.updateUrl(url);
    return super.put(url, body, this.getRequestOptionArgs(options)).catch(this.catchErrors());
  }

  public delete(url: string, options?: RequestOptionsArgs): Observable<Response> {
    url = this.updateUrl(url);
    return super.delete(url, this.getRequestOptionArgs(options)).catch(this.catchErrors());
  }

  private catchErrors() {
    return (response: Response) => {
      if (response.status === 0) {
        this.toastrService.error('Server is not accessible.Please try again');
      }else if (response.status === 500) {
        this.toastrService.error('Request failed.Please try again');
      }else if (response.status === 503) {
        this.toastrService.error('Request timeout.Please try again');
      }else if (response.status === 400) {
        this.toastrService.error('Request error.Please try again');
          this.toastrService.error('Unauthorized user, please login again.');
          // this.router.navigate(['/login/cvmaker']);
          return;
      }
      return Observable.throw(response);
    };
  }

  private updateUrl(req: string) {
    return 'http://localhost:3000/api' + req;
  }

  private getRequestOptionArgs(options?: RequestOptionsArgs): RequestOptionsArgs {
    if (options == null) {
      options = new RequestOptions();
    }
    if (options.headers == null) {
      options.headers = new Headers();
    }
    options.headers.append('Content-Type', 'application/json');
    return options;
  }

  private onSuccess(res: Response): void {
    console.log(res);
  }

}
