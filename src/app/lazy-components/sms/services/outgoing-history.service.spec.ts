import { TestBed, inject } from '@angular/core/testing';

import { OutgoingHistoryService } from './outgoing-history.service';

describe('OutgoingHistoryService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [OutgoingHistoryService]
    });
  });

  it('should be created', inject([OutgoingHistoryService], (service: OutgoingHistoryService) => {
    expect(service).toBeTruthy();
  }));
});
