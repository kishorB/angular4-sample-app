import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';

const mocData = {
  'record_list': [
    {
      'account_id': 80000340,
      'created_on': '2016-09-28 01:03:04',
      'id': 25,
      'incoming_provider': 'Aerial US',
      'keyword': 'anand',
      'media_url': '',
      'message': 'Test incoming message from screen magic',
      'mobile_number': '917875471177',
      'push_sf_response': 'User has both Oauth and Webservice Disabled',
      'push_sf_status': 'error',
      'push_url_response': 'testing',
      'push_url_status': 'success',
      'response': '1',
      'sf_id': 'weasdasd',
      'short_code': '16473170916',
      'sub_keyword': 'tripathi'
    },
    {
      'account_id': 80000341,
      'created_on': '2016-09-28 01:03:04',
      'id': 25,
      'incoming_provider': 'Aerial US',
      'keyword': 'anand',
      'media_url': '',
      'message': 'Test incoming message from screen magic',
      'mobile_number': '917875471177',
      'push_sf_response': 'User has both Oauth and Webservice Disabled',
      'push_sf_status': 'error',
      'push_url_response': 'testing',
      'push_url_status': 'success',
      'response': '1',
      'sf_id': 'weasdasd',
      'short_code': '16473170916',
      'sub_keyword': 'tripathi'
    },
    {
      'account_id': 80000342,
      'created_on': '2016-09-28 01:03:04',
      'id': 25,
      'incoming_provider': 'Aerial US',
      'keyword': 'anand',
      'media_url': '',
      'message': 'Test incoming message from screen magic',
      'mobile_number': '917875471177',
      'push_sf_response': 'User has both Oauth and Webservice Disabled',
      'push_sf_status': 'error',
      'push_url_response': 'testing',
      'push_url_status': 'success',
      'response': '1',
      'sf_id': 'weasdasd',
      'short_code': '16473170916',
      'sub_keyword': 'tripathi'
    },
    {
      'account_id': 80000343,
      'created_on': '2016-09-28 01:03:04',
      'id': 25,
      'incoming_provider': 'Aerial US',
      'keyword': 'anand',
      'media_url': '',
      'message': 'Test incoming message from screen magic',
      'mobile_number': '917875471177',
      'push_sf_response': 'User has both Oauth and Webservice Disabled',
      'push_sf_status': 'error',
      'push_url_response': 'testing',
      'push_url_status': 'success',
      'response': '1',
      'sf_id': 'weasdasd',
      'short_code': '16473170916',
      'sub_keyword': 'tripathi'
    }
    , {
      'account_id': 80000344,
      'created_on': '2016-09-28 01:03:04',
      'id': 25,
      'incoming_provider': 'Aerial US',
      'keyword': 'anand',
      'media_url': '',
      'message': 'Test incoming message from screen magic',
      'mobile_number': '917875471177',
      'push_sf_response': 'User has both Oauth and Webservice Disabled',
      'push_sf_status': 'error',
      'push_url_response': 'testing',
      'push_url_status': 'success',
      'response': '1',
      'sf_id': 'weasdasd',
      'short_code': '16473170916',
      'sub_keyword': 'tripathi'
    }
  ],
  'total_records': 5
};

@Injectable()
export class IncomingHistoryService {

  constructor(private http: Http) { }

  public getSMSHistory(queryString?, options?) {
    return mocData;
    /* let serverAPI = '/incoming_history';
    if (queryString) {
      serverAPI = serverAPI + queryString;
    }
    if (options) {
      return this.http.get(serverAPI, options).map((response) => response.json());
    }else {
      return this.http.get(serverAPI).map((response) => response.json());
    } */
  }
}
