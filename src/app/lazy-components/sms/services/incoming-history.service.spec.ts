import { TestBed, inject } from '@angular/core/testing';

import { IncomingHistoryService } from './incoming-history.service';

describe('IncomingHistoryService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [IncomingHistoryService]
    });
  });

  it('should be created', inject([IncomingHistoryService], (service: IncomingHistoryService) => {
    expect(service).toBeTruthy();
  }));
});
