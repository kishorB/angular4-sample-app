import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';

const mocData = {
  'record_list':
  [
    {
      'is_international': 1,
      'account_id': 80004482,
      'sent_status': 'success',
      'encoding': 1,
      'text': 'hello all this is a sms template',
      'sender_id': 'test',
      'campaign_run_id': 0,
      'credits': 1,
      'created_on': '2017-03-23 12:22:11',
      'dr_received_on': null,
      'mobile_number': '17149305641',
      'formatted_mobile_number': '17149305641',
      'delivery_status': 'delivered',
      'sf_external_field': '55caac41-0fc3-11e7-a660-0e3d7f731c04',
      'push_to_sf_response': null,
      'resend_on': null,
      'push_to_sf_status': null,
      'user_id': '0',
      'vendor_cost': 0,
      'country_id': 3,
      'service_provider_id': 391,
      'id': 15485472,
      'channel_type': 'sms',
      'provider_response_id': null,
      'response_id': '6b44130a-8be6-4589-8985-39772a5cd2a8',
      'media_url': '',
      'euro_credits': 0.02906,
      'long_sms_id': 0
    },
    {
      'is_international': 1,
      'account_id': 80004482,
      'sent_status': 'success',
      'encoding': 1,
      'text': 'hello all this is a sms template',
      'sender_id': 'test',
      'campaign_run_id': 0,
      'credits': 1,
      'created_on': '2017-03-23 12:22:11',
      'dr_received_on': null,
      'mobile_number': '17149305641',
      'formatted_mobile_number': '17149305641',
      'delivery_status': null,
      'sf_external_field': '55caac41-0fc3-11e7-a660-0e3d7f731c04',
      'push_to_sf_response': null,
      'resend_on': null,
      'push_to_sf_status': null,
      'user_id': '0',
      'vendor_cost': 0,
      'country_id': 3,
      'service_provider_id': 391,
      'id': 15485472,
      'channel_type': 'sms',
      'provider_response_id': null,
      'response_id': '6b44130a-8be6-4589-8985-39772a5cd2a8',
      'media_url': '',
      'euro_credits': 0.02906,
      'long_sms_id': 0
    },
    {
      'is_international': 1,
      'account_id': 80004482,
      'sent_status': 'success',
      'encoding': 1,
      'text': 'hello all this is a sms template',
      'sender_id': 'test',
      'campaign_run_id': 0,
      'credits': 1,
      'created_on': '2017-03-23 12:22:11',
      'dr_received_on': null,
      'mobile_number': '17149305642',
      'formatted_mobile_number': '17149305641',
      'delivery_status': null,
      'sf_external_field': '55caac41-0fc3-11e7-a660-0e3d7f731c04',
      'push_to_sf_response': null,
      'resend_on': null,
      'push_to_sf_status': null,
      'user_id': '0',
      'vendor_cost': 0,
      'country_id': 3,
      'service_provider_id': 391,
      'id': 15485472,
      'channel_type': 'sms',
      'provider_response_id': null,
      'response_id': '6b44130a-8be6-4589-8985-39772a5cd2a8',
      'media_url': '',
      'euro_credits': 0.02906,
      'long_sms_id': 0
    },
    {
      'is_international': 1,
      'account_id': 80004482,
      'sent_status': 'success',
      'encoding': 1,
      'text': 'hello all this is a sms template',
      'sender_id': 'test',
      'campaign_run_id': 0,
      'credits': 1,
      'created_on': '2017-03-23 12:22:11',
      'dr_received_on': null,
      'mobile_number': '17149305641',
      'formatted_mobile_number': '17149305641',
      'delivery_status': null,
      'sf_external_field': '55caac41-0fc3-11e7-a660-0e3d7f731c04',
      'push_to_sf_response': null,
      'resend_on': null,
      'push_to_sf_status': null,
      'user_id': '0',
      'vendor_cost': 0,
      'country_id': 3,
      'service_provider_id': 391,
      'id': 15485472,
      'channel_type': 'sms',
      'provider_response_id': null,
      'response_id': '6b44130a-8be6-4589-8985-39772a5cd2a8',
      'media_url': '',
      'euro_credits': 0.02906,
      'long_sms_id': 0
    },
    {
      'is_international': 1,
      'account_id': 80004482,
      'sent_status': 'success',
      'encoding': 1,
      'text': 'hello all this is a sms template',
      'sender_id': 'test',
      'campaign_run_id': 0,
      'credits': 1,
      'created_on': '2017-03-23 12:22:11',
      'dr_received_on': null,
      'mobile_number': '17149305643',
      'formatted_mobile_number': '17149305643',
      'delivery_status': null,
      'sf_external_field': '55caac41-0fc3-11e7-a660-0e3d7f731c04',
      'push_to_sf_response': null,
      'resend_on': null,
      'push_to_sf_status': null,
      'user_id': '0',
      'vendor_cost': 0,
      'country_id': 3,
      'service_provider_id': 391,
      'id': 15485472,
      'channel_type': 'sms',
      'provider_response_id': null,
      'response_id': '6b44130a-8be6-4589-8985-39772a5cd2a8',
      'media_url': '',
      'euro_credits': 0.02906,
      'long_sms_id': 0
    },
    {
      'is_international': 1,
      'account_id': 80004482,
      'sent_status': 'success',
      'encoding': 1,
      'text': 'hello all this is a sms template',
      'sender_id': 'test',
      'campaign_run_id': 0,
      'credits': 1,
      'created_on': '2017-03-23 12:22:11',
      'dr_received_on': null,
      'mobile_number': '17149305642',
      'formatted_mobile_number': '17149305641',
      'delivery_status': null,
      'sf_external_field': '55caac41-0fc3-11e7-a660-0e3d7f731c04',
      'push_to_sf_response': null,
      'resend_on': null,
      'push_to_sf_status': null,
      'user_id': '0',
      'vendor_cost': 0,
      'country_id': 3,
      'service_provider_id': 391,
      'id': 15485472,
      'channel_type': 'sms',
      'provider_response_id': null,
      'response_id': '6b44130a-8be6-4589-8985-39772a5cd2a8',
      'media_url': '',
      'euro_credits': 0.02906,
      'long_sms_id': 0
    },
    {
      'is_international': 1,
      'account_id': 80004482,
      'sent_status': 'success',
      'encoding': 1,
      'text': 'hello all this is a sms template',
      'sender_id': 'test',
      'campaign_run_id': 0,
      'credits': 1,
      'created_on': '2017-03-23 12:22:11',
      'dr_received_on': null,
      'mobile_number': '17149305641',
      'formatted_mobile_number': '17149305641',
      'delivery_status': null,
      'sf_external_field': '55caac41-0fc3-11e7-a660-0e3d7f731c04',
      'push_to_sf_response': null,
      'resend_on': null,
      'push_to_sf_status': null,
      'user_id': '0',
      'vendor_cost': 0,
      'country_id': 3,
      'service_provider_id': 391,
      'id': 15485472,
      'channel_type': 'sms',
      'provider_response_id': null,
      'response_id': '6b44130a-8be6-4589-8985-39772a5cd2a8',
      'media_url': '',
      'euro_credits': 0.02906,
      'long_sms_id': 0
    },
    {
      'is_international': 1,
      'account_id': 80004482,
      'sent_status': 'success',
      'encoding': 1,
      'text': 'hello all this is a sms template',
      'sender_id': 'test',
      'campaign_run_id': 0,
      'credits': 1,
      'created_on': '2017-03-23 12:22:11',
      'dr_received_on': null,
      'mobile_number': '17149305641',
      'formatted_mobile_number': '17149305641',
      'delivery_status': null,
      'sf_external_field': '55caac41-0fc3-11e7-a660-0e3d7f731c04',
      'push_to_sf_response': null,
      'resend_on': null,
      'push_to_sf_status': null,
      'user_id': '0',
      'vendor_cost': 0,
      'country_id': 3,
      'service_provider_id': 391,
      'id': 15485472,
      'channel_type': 'sms',
      'provider_response_id': null,
      'response_id': '6b44130a-8be6-4589-8985-39772a5cd2a8',
      'media_url': '',
      'euro_credits': 0.02906,
      'long_sms_id': 0
    },
    {
      'is_international': 1,
      'account_id': 80004482,
      'sent_status': 'success',
      'encoding': 1,
      'text': 'hello all this is a sms template',
      'sender_id': 'test',
      'campaign_run_id': 0,
      'credits': 1,
      'created_on': '2017-03-23 12:22:11',
      'dr_received_on': null,
      'mobile_number': '17149305641',
      'formatted_mobile_number': '17149305641',
      'delivery_status': null,
      'sf_external_field': '55caac41-0fc3-11e7-a660-0e3d7f731c04',
      'push_to_sf_response': null,
      'resend_on': null,
      'push_to_sf_status': null,
      'user_id': '0',
      'vendor_cost': 0,
      'country_id': 3,
      'service_provider_id': 391,
      'id': 15485472,
      'channel_type': 'sms',
      'provider_response_id': null,
      'response_id': '6b44130a-8be6-4589-8985-39772a5cd2a8',
      'media_url': '',
      'euro_credits': 0.02906,
      'long_sms_id': 0
    },
    {
      'is_international': 1,
      'account_id': 80004483,
      'sent_status': 'success',
      'encoding': 1,
      'text': 'hello all this is a sms template',
      'sender_id': 'test',
      'campaign_run_id': 0,
      'credits': 1,
      'created_on': '2017-03-23 12:22:11',
      'dr_received_on': null,
      'mobile_number': '17149305641',
      'formatted_mobile_number': '17149305641',
      'delivery_status': null,
      'sf_external_field': '55caac41-0fc3-11e7-a660-0e3d7f731c04',
      'push_to_sf_response': null,
      'resend_on': null,
      'push_to_sf_status': null,
      'user_id': '0',
      'vendor_cost': 0,
      'country_id': 3,
      'service_provider_id': 391,
      'id': 15485475,
      'channel_type': 'mms',
      'provider_response_id': null,
      'response_id': '6b44130a-8be6-4589-8985-39772a5cd2a8',
      'media_url': '',
      'euro_credits': 0.02906,
      'long_sms_id': 0
    }
  ],
  'total_records': 23
};

@Injectable()
export class OutgoingHistoryService {

  constructor(private http: Http) { }

  public getSMSHistory(queryString?, options?) {
    return mocData;
    /* let serverAPI = '/outgoing_history';
    if (queryString) {
      serverAPI = serverAPI + queryString;
    }
    if (options) {
      return this.http.get(serverAPI, options).map((response) => response.json());
    }else {
      return this.http.get(serverAPI).map((response) => response.json());
    } */
  }
}


