import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { IncomingHistoryComponent } from './components/incoming-history/incoming-history.component';
import { OutgoingHistoryComponent } from './components/outgoing-history/outgoing-history.component';

const routes: Routes = [
  { path: 'incomingSMSHistory', component: IncomingHistoryComponent },
  { path: 'outgoingSMSHistory', component: OutgoingHistoryComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SmsRoutingModule { }
