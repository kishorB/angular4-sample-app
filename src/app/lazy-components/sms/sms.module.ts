import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpModule, Http, XHRBackend, RequestOptions } from '@angular/http';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { httpFactory } from '../../shared/http.factory';

import { SmsRoutingModule } from './sms-routing.module';

import { IncomingHistoryComponent } from './components/incoming-history/incoming-history.component';
import { OutgoingHistoryComponent } from './components/outgoing-history/outgoing-history.component';
import { IncomingHistoryService, OutgoingHistoryService } from './services';


@NgModule({
  imports: [
    CommonModule,
    SmsRoutingModule,
    HttpModule,
    FormsModule
  ],
  providers: [
    IncomingHistoryService,
    OutgoingHistoryService,
    {
      provide: Http,
      useFactory: httpFactory,
      deps: [XHRBackend, RequestOptions , ToastrService , Router]
    }
  ],
  declarations: [
    IncomingHistoryComponent,
    OutgoingHistoryComponent
  ],
  exports: [
    IncomingHistoryComponent,
    OutgoingHistoryComponent
  ]
})
export class SmsModule { }
