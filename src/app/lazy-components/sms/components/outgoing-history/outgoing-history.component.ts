import { Component, OnInit, TemplateRef } from '@angular/core';
import { OutgoingHistoryService } from '../../services';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';
declare let $: any;

@Component({
  selector: 'app-outgoing-history',
  templateUrl: './outgoing-history.component.html',
  styleUrls: ['./outgoing-history.component.css']
 })

export class OutgoingHistoryComponent implements OnInit {

  public outgoingSmsHistoryData: any;
  public criteria: Object = {channel_type: 0};
  public modalRef: BsModalRef;

  constructor(public outgoingHistoryService: OutgoingHistoryService,
    public modalService: BsModalService) {
    this.getSmsHistory();
  }

  public openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, {'ignoreBackdropClick': true, keyboard: false});
  }

  private getSmsHistory() {
    this.outgoingSmsHistoryData = this.outgoingHistoryService.getSMSHistory(null);
    // console.log('=============   DATA RECIVIED =============');
    // console.log(this.outgoingSmsHistoryData);
  }

  public onSubmit() {
    console.log('form submitted====' + JSON.stringify(this.criteria));
  }

  public ngOnInit(): void {
    $('.datepicker').datepicker();
  }
}
