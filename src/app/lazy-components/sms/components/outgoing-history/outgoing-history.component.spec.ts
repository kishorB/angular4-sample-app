import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OutgoingHistoryComponent } from './outgoing-history.component';

describe('OutgoingHistoryComponent', () => {
  let component: OutgoingHistoryComponent;
  let fixture: ComponentFixture<OutgoingHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OutgoingHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OutgoingHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
