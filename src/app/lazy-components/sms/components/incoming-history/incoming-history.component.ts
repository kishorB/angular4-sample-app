import { Component, OnInit, TemplateRef } from '@angular/core';
import { IncomingHistoryService } from '../../services';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';
declare let $: any;

@Component({
  selector: 'app-incoming-history',
  templateUrl: './incoming-history.component.html',
  styleUrls: ['./incoming-history.component.css']
})

export class IncomingHistoryComponent implements OnInit {

  public incomingSmsHistoryData: any;
  public criteria: Object = { channel_type: 0 };
  public modalRef: BsModalRef;

  constructor(public incomingHistoryService: IncomingHistoryService,
    public modalService: BsModalService) {
    this.getResponsesData();
  }

  public openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }

  private getResponsesData() {
    this.incomingSmsHistoryData = this.incomingHistoryService.getSMSHistory();
    // console.log('=============   DATA RECIVIED =============');
    // console.log(this.incomingSmsHistoryData);
  }

  public onSubmit(form) {
    // form.reset();
    console.log('form submitted====' + JSON.stringify(this.criteria));
  }

  ngOnInit() {
    $('.datepicker').datepicker();
  }
}
