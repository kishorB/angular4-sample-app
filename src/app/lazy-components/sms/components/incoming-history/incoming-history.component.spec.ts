import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IncomingHistoryComponent } from './incoming-history.component';

describe('IncomingHistoryComponent', () => {
  let component: IncomingHistoryComponent;
  let fixture: ComponentFixture<IncomingHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IncomingHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IncomingHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
