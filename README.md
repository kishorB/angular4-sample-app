# TESTAPP

To setup node and npm in this project [nvm](https://github.com/creationix/nvm) is used.
This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.4.0.

## Setting node and npm version

Run `nvm use` command to set node and npm version (Uses .nvmrc file)

Run `npm install` to install node modules.

## Development server

Run `npm start` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `npm run build-dev` to build the project for developement environment.

Run `npm run build-prod` to build the project for production environment.

The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `npm run test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `npm run e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Before running the tests make sure you are serving the app via `npm start`.

## CI

## Code features implemented in this application is as follows.

1) Project structure as suggested by angular style guide.  
2) Modularity of components.  
3) Build configuration.(`Dev` as well as `Prod`)  
4) Way to add third party libraries in application.(`Bootstrap`,`Jquery`,`Jquery UI`)  
5) End to end test suite integrated.(Uses `protractor`)  
6) Unit testing suite intgrated.(Uses `karma`)  
7) Component **lazy loading**. (Helps to divide main bundle into smaller chunks)  
8) Routing structure.  
9) Global __HTTP interceptor__ written for application, which helps to manage http operations like handling server error in one place.  
10) .nvmrc helps to set node and npm version for application.  
11) Linting of project is also done. ng-cli uses `codelyzer` internally which is suggested by designer of angular style guide.  
12) Organization of shared components, so that they can be accessed anywhere in application.  
13) Application documentation generator is also integrated (Uses `compodoc`). This will help team to understand application structute and all internal modules.  
14) Proper way to manage assets.(Using `.angular-cli.json`).  
15) For production build we can check license of libraries used in application.